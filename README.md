# Dynamic Cloud

## Leírás
A Dynamic Cloud egy adatvizualizációs projekt, ami évről évre egy diamikusan változó szófelhőben mutatja a trendeket. A szavak TED-Talks előadásokból származnak és minél többször fodultak elá annál nagyobbak.
Videó a szófelhőről: https://www.youtube.com/watch?v=eN00Z7ISa-Q

data_process.py: Az adatbázisból kinyeri a szükséges adatokat

generate_wordcloud.py: Videót generál a feldolgozott adatok alapján

wordcloud_gui.py: egy nagyon egyszerű grafikus interfész, a videó már generálás közben látható (nem tökéletes pl.: néha össze akad open cv-vel)

## Források
Adatbázis: https://www.kaggle.com/miguelcorraljr/ted-ultimate-dataset

A programhoz felhasználtam és átalakítottam egy meglévő csomagot, amit word_cloud-nak hívnak.
Változtatásaim a wordcloud.py fájlban találhatók.
https://github.com/amueller/word_cloud
