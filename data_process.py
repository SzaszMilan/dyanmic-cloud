import pandas as ps
import nltk
import csv
import datetime
import operator
import time

start_time = time.time()
data = ps.read_csv('ted_talks_en.csv', encoding='utf8')
data['recorded_date'] = ps.to_datetime(data['recorded_date'])
data = data.sort_values(by='recorded_date')
data_len = len(data)

begin = data.iloc[0]['recorded_date']
begin = datetime.datetime(begin.year, begin.month, begin.day)
delta = datetime.timedelta(days=365)
new_data = []
freq = nltk.FreqDist()

stopwordSet = set(nltk.corpus.stopwords.words('english'))
with open('stopwords', 'r', encoding='utf8') as f:
    for word in f:
        stopwordSet.add(word.rstrip('\n'))

max_word = 20
min_freq = 20

with open('new_file.csv', 'w', encoding='utf-8', newline='') as new_file:
    field_names = ['begin_date', 'ending_date', 'freqs']
    csv_writer = csv.DictWriter(new_file, field_names)
    csv_writer.writeheader()
    for i, date in enumerate(data['recorded_date']):
        print("{}% done".format(round(i/data_len*100)))

        if date >= begin and date < begin + delta:
            begin = begin
        else:
            if freq.B() >= 5:
                freq = sorted(freq.items(), key=operator.itemgetter(1), reverse=True)[:max_word]
                csv_writer.writerow({'begin_date': begin.strftime("%Y/%m/%d"), 'ending_date': (begin + delta).strftime("%Y/%m/%d"), 'freqs': freq})

            begin = data.iloc[i]['recorded_date']
            freq = nltk.FreqDist()
        
        tokens = nltk.word_tokenize(data.iloc[i]['transcript'])
        tokens = [x.lower() for x in tokens if x.lower() not in stopwordSet]
        new_freq = nltk.FreqDist(tokens)

        for key in list(new_freq.keys()):
            if new_freq[key] <= min_freq:
                del new_freq[key]

        freq += dict(sorted(new_freq.items(), key=operator.itemgetter(1), reverse=True)[:max_word])

print(time.time() - start_time)
