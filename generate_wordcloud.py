import pandas as ps
import nltk
from operator import itemgetter
from wordcloud import WordCloud, STOPWORDS
import operator
import time
from ast import literal_eval

start_time = time.time()

data = ps.read_csv('new_file.csv', encoding='utf8')
data_len = len(data)
data_to_do = data_len #shrink to fasten generating

minimum = 15
maximum = 15

for i,line in enumerate(data['freqs']):
    line_dict = dict(literal_eval(line))
    for value in list(line_dict.values()):
        if value < minimum:
            minimum = value
        if value > maximum:
            maximum = value

wordcloud = WordCloud(minimum, maximum, width=1920, height=1080, background_color='white', min_font_size= 18, max_font_size=200)
for i,line in enumerate(data['freqs'][:data_to_do]):
    print("{}% done".format(round(i/data_to_do*100)))
    wordcloud.generate_from_frequencies(dict(literal_eval(line)))
    wordcloud.to_file(data['begin_date'][i], data['ending_date'][i])

print("--- Image generation: %s seconds ---" % (time.time() - start_time))

wordcloud.generate_video()
