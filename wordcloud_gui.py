from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PIL.ImageQt import ImageQt
import sys
import asyncio
from qasync import QEventLoop, QThreadExecutor, asyncSlot
import time
import numpy as np
import pandas as ps
from ast import literal_eval
from wordcloud import WordCloud, STOPWORDS


class Worker(QRunnable):
    def __init__(self, img_array, wordcloud, data, data_to_do):
        super(Worker, self).__init__()
        self.img_array = img_array
        self.wordcloud = wordcloud
        self.data = data
        self.data_to_do = data_to_do

    @pyqtSlot()
    def run(self):
        try:
            for i,line in enumerate(self.data['freqs'][:self.data_to_do]):
                new_images = []
                self.wordcloud.generate_from_frequencies(dict(literal_eval(line)))
                self.wordcloud.to_image_array(self.data['begin_date'][i], self.data['ending_date'][i], new_images)
                self.img_array.append(new_images)
                print(i)
        except Exception as e:
            print(e)


class ImagePlayer(QMainWindow):
    def __init__(self):
        super(ImagePlayer, self).__init__()
        self.threadpool = QThreadPool()
        self.initUi()
        self.initWordCloud()

    def initWordCloud(self):
        self.data = ps.read_csv('new_file.csv', encoding='utf8')
        data_len = len(self.data)
        self.data_to_do = data_len

        minimum = 15
        maximum = 15

        for i,line in enumerate(self.data['freqs']):
            line_dict = dict(literal_eval(line))
            for value in list(line_dict.values()):
                if value < minimum:
                    minimum = value
                if value > maximum:
                    maximum = value

        self.wordcloud = WordCloud(minimum, maximum, width=1422, height=800, background_color='white', min_font_size= 50, max_font_size=160)

    def initUi(self):
        img = QPixmap("background.png")
        self.currentPicture = 0
        self.currentArray = 0
        self.controlArea = 300
        self.playing = False
        self.generating = False
        self.img_array = []

        self.setFixedSize(img.width() + self.controlArea, img.height())

        self.controlLabel = QLabel(self)
        self.controlLabel.setGeometry(0, 0, self.controlArea, 100)
        self.controlLabel.setText("Controls")
        self.controlLabel.setAlignment(Qt.AlignCenter)
        self.controlLabel.setFont(QFont("Times", 50))

        self.playButton = QPushButton(self)
        self.playButton.setGeometry(100,100, 50, 50)
        self.playButton.setText("Play")
        self.playButton.clicked.connect(self.startAnimation)

        self.imageLabel = QLabel(self)
        self.imageLabel.setPixmap(img)
        self.imageLabel.setGeometry(self.controlArea, 0, img.width(), img.height())

        self.timer = QTimer()
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.changePicture)

    def startAnimation(self):
        if self.playing:
            self.playing = False
            self.timer.stop()
            self.playButton.setText("Start")
        else:
            self.playing = True
            self.playButton.setText("Stop")
            if not self.generating:
                self.worker = Worker(self.img_array, self.wordcloud, self.data, self.data_to_do)
                self.threadpool.start(self.worker)
                self.startTime = time.time()
                self.generating = True
            self.timer.start()

    def changePicture(self):
        if time.time() - self.startTime >= 5: 
            try:              
                self.arrayChecker()  
                img = ImageQt(self.img_array[self.currentArray][self.currentPicture])                                                                                                                                                 
                img = QPixmap.fromImage(img)
                self.currentPicture += 1
                self.imageLabel.setPixmap(img)
                del img
            except Exception as e:
                print(e)
                self.timer.stop()

    def arrayChecker(self):
        if self.currentPicture == len(self.img_array[self.currentArray]):
            self.img_array[self.currentArray].clear()
            self.currentArray += 1
            self.currentPicture = 0
            

    def closeEvent(self, *args, **kwargs):
        super(QMainWindow, self).closeEvent(*args, **kwargs)
        for array in self.img_array:
            print(len(array))

def window():
    app = QApplication(sys.argv)
    win = ImagePlayer()

    win.show()
    sys.exit(app.exec_())

window()